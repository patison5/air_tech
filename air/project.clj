(defproject air "0.0.1"
  :description "School project"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [ring "1.4.0"]
                 [ring/ring-defaults "0.1.5"]
                 [compojure "1.4.0"]
                 [enlive "1.1.6"]
                 [org.clojure/java.jdbc "0.4.2"]
                 [mysql/mysql-connector-java "5.1.6"]
                 [hiccup "1.0.5"]
                 [clj-jade "0.1.7"]]
  :plugins [[lein-ring "0.9.7"]]
  :ring {:handler air.core/app}
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
