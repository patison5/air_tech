(ns air.base
  (:require [clojure.java.jdbc :as sql]
            [compojure.core :refer :all]
            [hiccup.core :refer :all]
            [hiccup.page :refer :all]
            [ring.util.anti-forgery :refer [anti-forgery-field]]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [air.database :refer [db]])
    (:use clojure.java.io))
   
(defn create-db-structure []
   "Data structure"
   
   (sql/execute! db ["drop table if exists planes"])
   (sql/execute! db ["drop table if exists users"])
   (sql/execute! db ["drop table if exists service_type"])
   (sql/execute! db ["drop table if exists use_log"])
   (sql/execute! db ["drop table if exists pilots"])
   (sql/execute! db ["drop table if exists employees"])
   (sql/execute! db ["drop view if exists planes_hours"])
   
;   (sql/query db ["SET NAMES 'utf8'"])
;   (sql/query db ["SET CHARACTER SET 'utf8'"])
;   (sql/query db ["SET SESSION collation_connection = 'utf8_general_ci'"])

   
    ; (sql/execute! db ["ALTER TABLES CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;"])
    ; (sql/query db ["SET NAMES utf8 COLLATE utf8_unicode_ci"])
   
    ; ALTER DATABASE airdb CHARACTER SET utf8 COLLATE utf8_unicode_ci;

;   collation_connection
   
   "Create tables"
   (sql/db-do-commands db
    (sql/create-table-ddl :planes
                          [:plane_id "integer" "PRIMARY KEY" "AUTO_INCREMENT"]
                          [:plane_name "varchar(255)"]
                          [:plane_number "integer"]
                          [:creation_date "date"]
                          [:last_updated "date"]
                          [:air_time "integer"]
                          [:landing_count "integer"]
                          [:plane_status "varchar(255)"])
                      
    (sql/create-table-ddl :users
                          [:user_id "integer" "PRIMARY KEY" "AUTO_INCREMENT"]
                          [:user_name "varchar(255)"]
                          [:user_surname "varchar(255)"]
                          [:user_email "varchar(255)"]
                          [:user_role "varchar(255)"]
                          [:user_password "varchar(255)"]
                          [:status "varchar(255)"])
                      
    (sql/create-table-ddl :employees
                          [:emp_id "integer" "PRIMARY KEY" "AUTO_INCREMENT"]
                          [:emp_name "varchar(255)"]
                          [:emp_surname "varchar(255)"]
                          [:patronymic "varchar(255)"]
                          [:emp_age "integer"]
                          [:emp_spec "varchar(255)"]
                          [:emp_email "varchar(255)"]
                          [:emp_standing "integer"]
                          [:emp_number "varchar(255)"]
                          [:emp_country "varchar(255)"]
                          [:emp_town "varchar(255)"]
                          [:emp_education "TEXT"]
                          [:emp_exp "TEXT"]
                          [:emp_skills "TEXT"]
                          [:emp_pasport "varchar(255)"]
                          [:get_date "date"]
                          [:out_date "varchar(255)"])

    (sql/create-table-ddl :service_type
                          [:service_id "integer" "PRIMARY KEY" "AUTO_INCREMENT"]
                          [:service_name "varchar(255)"]
                          [:h_regularity "integer"]
                          [:l_regularity "integer"]
                          [:description "varchar(255)"])
                      
    (sql/create-table-ddl :use_log
                          [:log_id "integer" "PRIMARY KEY" "AUTO_INCREMENT"]
                        ;   [:plane_id "integer" "references planes (plane_id)"]
                          [:pl_number "integer" "references planes (plane_number)"]
                          [:pilot "varchar(255)"]
                          [:s_pilot "varchar(255)"]
                          [:use_date "date"]
                          [:use_time "integer"]
                          [:squawk "TEXT"])
    
    (sql/create-table-ddl :pilots
                          [:pilot_id "integer" "PRIMARY KEY" "AUTO_INCREMENT"]
                          [:pilot_name "integer" "references planes (plane_id)"]
                          [:birth_date "date"]
                          [:use_time "integer"])))
    
    
    
(defn base-planes []
    "добавляем основные самолеты в базу"
    (sql/insert! db :planes
       {:plane_name "Piper Seneca V PA34"            :plane_number "12451547" 	 :creation_date "2001-04-18"    :last_updated "2012-02-01" :air_time "232" :landing_count "2"  :plane_status "ready"}
       {:plane_name "Be-76 Duches"                   :plane_number "544571"      :creation_date "2012-02-13"    :last_updated "2012-02-01" :air_time "120" :landing_count "24" :plane_status "ready"}
       {:plane_name "Beechcraft G58 Baron"           :plane_number "858745"      :creation_date "2000-08-13"    :last_updated "2012-02-01" :air_time "150" :landing_count "15" :plane_status "ready"}
       {:plane_name "Cessna T310R"                   :plane_number "43454453"    :creation_date "2015-04-19"    :last_updated "2012-02-01" :air_time "300" :landing_count "11" :plane_status "ready"}
       {:plane_name "L-410"                          :plane_number "548745"      :creation_date "2016-04-03"    :last_updated "2012-02-01" :air_time "234" :landing_count "15" :plane_status "ready"}
       {:plane_name "Morava L-200D"                  :plane_number "54545223"    :creation_date "2008-06-23"    :last_updated "2012-02-01" :air_time "343" :landing_count "23" :plane_status "ready"}
       {:plane_name "Amphibia L-42"                  :plane_number "29814373"    :creation_date "2011-03-25"    :last_updated "2012-02-01" :air_time "234" :landing_count "15" :plane_status "ready"}
       {:plane_name "EV-55 Outback"                  :plane_number "23738483"    :creation_date "2015-03-25"    :last_updated "2012-02-01" :air_time "201" :landing_count "14" :plane_status "ready"}
       {:plane_name "Technam P2006T"                 :plane_number "23983292"    :creation_date "1994-03-25"    :last_updated "2012-02-01" :air_time "656" :landing_count "9"  :plane_status "ready"}
       {:plane_name "Cherokee Piper Arrow II PA28"   :plane_number "3984392"     :creation_date "2016-04-03"    :last_updated "2012-02-01" :air_time "234" :landing_count "15" :plane_status "ready"}
       {:plane_name "Cessna 180RG"                   :plane_number "9834993"     :creation_date "2008-06-23"    :last_updated "2012-02-01" :air_time "343" :landing_count "23" :plane_status "ready"}
       {:plane_name "Remos GX"                       :plane_number "87433433"    :creation_date "2011-03-25"    :last_updated "2012-02-01" :air_time "234" :landing_count "15" :plane_status "ready"}
       {:plane_name "Mooney M20M"                    :plane_number "34343434"    :creation_date "2015-03-25"    :last_updated "2012-02-01" :air_time "201" :landing_count "14" :plane_status "ready"}
       {:plane_name "Cessna 209"                     :plane_number "34343423"    :creation_date "1994-03-25"    :last_updated "2012-02-01" :air_time "656" :landing_count "9"  :plane_status "ready"}))
   
(defn base-services []
    "добавляем основные сервисы в базу"
    (sql/insert! db :service_type
       {:service_name "F-1"     :h_regularity "50"      :l_regularity "0"        :description "Oil Change / Analysis"}
       {:service_name "F-2"     :h_regularity "100"     :l_regularity "800"      :description "Compression Check"}
       {:service_name "F-3"     :h_regularity "300"     :l_regularity "1200"     :description "ELT battery replacement"}
       {:service_name "F-4"     :h_regularity "400"     :l_regularity "1600"     :description "Annual Check"}
       {:service_name "F-5"     :h_regularity "500"     :l_regularity "2000"     :description "Engine Overhaul"}
       {:service_name "F-6"     :h_regularity "600"     :l_regularity "2400"     :description "static system and altimeter, transponder, and VOR equipment checks"}))
   
(defn base-logs []
    "добавляем основные логи в базу"
    (sql/insert! db :use_log
       {:pl_number "12451547"      :pilot "Andrew Watson"       :s_pilot "Roma Beorn"           :use_date "2001-01-10"  :use_time "148"   :squawk "none"}
       {:pl_number "544571"        :pilot "Vasya Pupkin"        :s_pilot "Anwe Linkoln"         :use_date "2001-01-10"  :use_time "148"   :squawk "none"}
       {:pl_number "43454453"      :pilot "Ashot Vasgenovich"   :s_pilot "Geourge Kust"         :use_date "2001-01-10"  :use_time "148"   :squawk "none"}
       {:pl_number "43454453"      :pilot "Vasya Pupkin"        :s_pilot "Massagist Genadii"    :use_date "2001-01-10"  :use_time "148"   :squawk "none"}))
   

(defn create_view_sum_hours []
    "создаем view на просмотр суммы ячеек"
    (sql/query db ["CREATE VIEW sum_hours AS SELECT sum(use_time)  AS sum_plane, pl_number  FROM use_log GROUP BY pl_number;"]))
   
(defn add-plane [p_name, p_number, p_cr_date, pl_l_upd, a_time, l_count, status]
    "Функция добавления самолета в базу"
    (sql/insert! db :planes
       {:plane_name p_name  :plane_number p_number 	:creation_date p_cr_date :last_updated pl_l_upd :air_time a_time :landing_count l_count :plane_status status}))
   
   
(defn add-user [user_name, user_surname, user_email, user_role, user_password]
    "Функция добавления пользователя в базу"
    (sql/insert! db :users
       {:user_name user_name  :user_surname user_surname  :user_email user_email  :user_role user_role 	:user_password user_password  :status "offline"}))
    

(defn add-log [pl_number pilot s_pilot use_date use_time squawk]
    "добавляем логи в базу"
    (sql/insert! db :use_log
       {:pl_number pl_number  :pilot pilot  :s_pilot s_pilot  :use_date use_date  :use_time use_time :squawk (if (= squawk "") "none" squawk)}))
   
(defn add-empl [em_name em_surname patr emp_age em_spec em_email standing number 
                country town education exp skills pasport get_date out_date]
    "добавляем нового рабочего в базу"
    (sql/insert! db :employees
       {:emp_name em_name  :emp_surname em_surname :patronymic patr :emp_age emp_age :emp_spec em_spec :emp_email em_email :emp_standing standing :emp_number number 
        :emp_country country :emp_town town :emp_education education :emp_exp exp :emp_skills skills :emp_pasport pasport :get_date get_date :out_date (if (= out_date "") "none" out_date)}))
   
   
(defn update-empl 
  [id em_name em_surname patr emp_age em_spec em_email get_date out_date]
 "меняем время самолета пользователя"
        (sql/update! db :employees {:employee_name em_name  :employee_surname em_surname :patronymic patr :emp_age emp_age :employee_spec em_spec :employee_email em_email :get_date get_date :out_date (if (= out_date "") "none" out_date)} ["employee_id = ?" id]))

(defn update-user 
  [id Uname surname role email pass]
  "меняем пользователя"
        (sql/update! db :users {:user_name Uname  :user_surname surname  :user_email email  :user_role role  :user_password pass} ["user_id = ?" id]))
   
(defn update-log
    [plane_number pilot s_pilot use_date use_time]
    (sql/update! db :use_log { :pl_number plane_number :pilot pilot :s_pilot s_pilot :use_date use_date :use_time use_time}))

; (defn join-plane_log
;     []
;     (sql/query db "SELECT * FROM planes LEFT JOIN use_log  ON (planes.plane_id = use_log.plane_id);"))

(defn install 
    []
    (create-db-structure)
    (base-planes)
    (base-services)
    (base-logs)
    (add-plane "Boeing 777" "1234213" "1932-01-15" "2014-12-13" "100" "13" "ready")
    (add-user "Fedor" "Penin" "patison4@yandex.ru" "admin" "250299")
    ; (sum-plane_hours)
    )


