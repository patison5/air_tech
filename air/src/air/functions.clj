(ns air.functions
  (:require
    [clojure.java.jdbc :as sql]
    [air.database :refer [db]]
    [air.planes :refer :all]
    [ring.util.response :refer :all]))


(defn online
    [email]
    (sql/update! db :users {:status "online"} ["user_email = ?" email]))

(defn offline
    [email]
    (sql/update! db :users {:status "offline"} ["user_email = ?" email]))

(defn logout []
    (assoc (redirect "/") :session {:mail "" :password ""}))


(defn to_hours
    [all_time]
    (int (/ (+ all_time 0.0) 60)))

(defn to_minutes
    [all_time]
    (int (- (+ all_time 0.0) (* (int (/ (+ all_time 0.0) 60)) 60))))

(defn show_hours
    [plane_id]
    "выводит часы налетанных часов"        
        (let [atime  (get (nth (get-plane_char plane_id) 0 "0") :air_time  "0")
              splane (get (nth (get-plane_char plane_id) 0 "0") :sum_plane "0")]

            (if (= splane nil) (def splane 0) splane)
            (int (/ (+ splane atime 0.0) 60))))
    
(defn show_minutes
    [plane_id]
    "выводит минуты налетанных часов"
        
    (let [atime  (get (nth (get-plane_char plane_id) 0 "0") :air_time  "0")
          splane (get (nth (get-plane_char plane_id) 0 "0") :sum_plane "0")]

        (if (= splane nil) (def splane 0) splane)
        (int (- (+ atime splane 0.0) (* (int (/ (+ atime splane 0.0) 60)) 60)))))


; (defn check_for-tech [a b c]
;     (def air_time  (. Integer parseInt  a))
;     (def sum_plane (. Integer parseInt  b))
;     (def use_time  (. Integer parseInt  c))
    
;     (cond
;         (and (< (+ air_time sum_plane)  100) (> (+ air_time sum_plane use_time) 100)) (println "необходимо технической офслуживания F-1")
;         (and (< (+ air_time sum_plane)  200) (> (+ air_time sum_plane use_time) 200)) (println "необходимо технической офслуживания F-2")
;         (and (< (+ air_time sum_plane)  300) (> (+ air_time sum_plane use_time) 300)) (println "необходимо технической офслуживания F-3")
;         (and (< (+ air_time sum_plane)  400) (> (+ air_time sum_plane use_time) 400)) (println "необходимо технической офслуживания F-4")
;         (and (< (+ air_time sum_plane)  500) (> (+ air_time sum_plane use_time) 500)) (println "необходимо технической офслуживания F-5")
;         (and (< (+ air_time sum_plane)  600) (> (+ air_time sum_plane use_time) 600)) (println "необходимо технической офслуживания F-6")
;         :true (println "Т.О. проводить не нужно")))




; "SELECT SUM(use_time)  AS sum_plane, pl_number  FROM use_log GROUP BY pl_number;"




; (let [c (+ 1 2)
;              [d e] [5 6]] 
;          (-> (+ d e) (- c)))


; (defn fib [n]
;   (if (<= n 1) n (+ (fib (dec n)) (fib (- n 2))))
;   (println n))



; (defn fib [f]
;   (let [mem (atom {})]
;     (fn [& args]
;       (if-let [e (find @mem args)]
;         (val e)
;         (let [ret (apply f args)]
;           (swap! mem assoc args ret)
;           ret)))))

; (defn fib [n]
;     (let [mem (atom {})]
;         (fn [& args]
;             (if-let [e (find @mem args)]
;                 (val e)
;                 (let [ret (apply n args)]
;                     (swap! mem assoc args ret)
;                     ret)))))