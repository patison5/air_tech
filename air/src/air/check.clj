(ns air.check
    (:require [clojure.java.jdbc :as sql]
              [compojure.core :refer :all]
              [hiccup.core :refer :all]
              [hiccup.page :refer :all]
              [ring.util.anti-forgery :refer [anti-forgery-field]]
              [clojure.edn :as edn]
              [clojure.java.io :as io]
              [air.database :refer [db]])
    (:use clojure.java.io))


(defn select-plane
    [numb]
    "check and add time in tables"
    (sql/query db ["SELECT SUM(use_time) from use_log where pl_number=?" numb]))

(defn change-plane 
  [numb]
 "меняем время самолета пользователя"
        (sql/update! db :planes {:air_time (select-plane numb)} ["pl_number = ?" numb]))

; (check-plane "544571")

; SELECT SUM(use_time) from use_log where pl_number='544571'