(ns air.delete_items
	(:require
    [clojure.java.jdbc :as sql]
    [air.database :refer [db]]))

(defn delete_worker [id]
	; (execute! db ["DELETE FROM employees WHERE emp_id =?" id])
	(sql/delete! db :employees ["emp_id = ?" id]))
