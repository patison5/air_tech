(ns air.layout
  (:require [net.cgrand.enlive-html :as html]
            [clj-jade.core :as jade]))

(jade/configure {:template-dir "resources/public/jade_templates/"
                 :pretty-print true
                 :cache? true})


(defn index-page []
    (jade/render "/index.jade" ))

(defn plane-page [planes]
    (jade/render "/planes.jade" {"planes" planes}))
    
(defn to_work-page [services]
    (jade/render "/to_work.jade" {"services" services}))

(defn add_plane-page []
    (jade/render "/add_plane-page.jade"))

(defn change_plane-gage []
    (jade/render "/change_plane-page.jade"))

(defn peolple-page [users employees]
    (jade/render "/people_page.jade" {"users" users "employees" employees}))

(defn add_user [users]
    (jade/render "/add_user.jade" {"users" users}))

(defn logs-page [logs]
    (jade/render "/logs_page.jade" {"logs" logs}))

(defn add_change-logs []
    (jade/render "/add_change_logs-page"))

(defn add_worker [employees]
    (jade/render "/add_worker" {"employees" employees}))

(defn registration []
    (jade/render "/registration"))

(defn login []
    (jade/render "/login"))

(defn single_person [employees]
    (jade/render "/single-people_page.jade" {"employees" employees}))

(defn single_plane [plane logs hours minutes]
    (jade/render "/single-plane_page.jade" {"plane" plane 
                                            "logs" logs 
                                            "sumh" [{"hours" hours "minutes" minutes}]}))

(defn page-404 []
    (jade/render "/page_404.jade"))

(defn admin-mysql-is-not-connected []
    (jade/render "/db_is_not_conn.jade"))