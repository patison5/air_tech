(ns air.core
  (:require
    [hiccup.core :refer :all]
    [hiccup.page :refer :all]
    [compojure.core :refer :all]
    [compojure.handler :as handler]
    [compojure.route :as route]
    [ring.util.anti-forgery :refer [anti-forgery-field]]
    [ring.middleware.session :refer [wrap-session]]
    [ring.middleware.session.memory :refer [memory-store]]
    [ring.middleware.defaults :refer :all]
    [ring.util.response :refer :all]
    [clojure.pprint :refer :all]
    [clojure.java.io :as io]
    [clojure.java.jdbc :as sql]
    [clj-jade.core :refer :all]
    [air.base :refer :all]
    [ring.middleware.params :refer [wrap-params]]
    [air.database :refer [db check-conn]]
    [air.layout :as layout]
    [air.planes :refer :all]
    [air.functions :refer :all]
    [air.delete_items :as del_it]
    ; [air.check :refer :all]
    )
  (:gen-class))


; (defn change-plane 
;   [numb]
; "меняем время самолета пользователя"
;         (sql/update! db :planes 
;                     {:air_time ((Integer/parseInt (re-find #"\A-?\d+" ["SELECT SUM(use_time) from use_log where pl_number=?" numb]))} 
;                       ["plane_number = ?" numb])))


; (defn selectnumber
;   []
;     {:find [?sum]})

; Это временные функции
(defn select-plane
    [numb]
    "check and add time in tables"
    (sql/query db ["SELECT SUM(use_time) As sum from use_log where pl_number=?" numb])
    ; (selectnumber)
    )

(defn change-plane 
  [numb]
 "меняем время самолета пользователя"
        (sql/update! db :planes {:air_time (select-plane numb)} ["plane_number = ?" numb]))
        
        


(defroutes myroutes
    (GET "/" [] 
         (if 
             (check-conn)
             (layout/index-page)
             (layout/admin-mysql-is-not-connected)))
    
    (GET "/planes" []
         (layout/plane-page (get-planes)))
       
    (GET "/services" []
         (layout/to_work-page (get-services)))
       

    (GET "/admin_add_p_l" []
         (layout/add_plane-page))
       
    (GET "/peolple-page" []
         (layout/peolple-page (get_users) (get_workers)))
    
    (GET "/logs" []
         (layout/logs-page (get_logs)))
    
    (GET "/add_change-logs" []
         (layout/add_change-logs ))
    
    (GET "/login" []
         (layout/login))
     
    (GET "/registration" []
         (layout/registration))
       
    (GET "/add_worker" []
         (layout/add_worker (get_workers)))
    
    (GET "/add_user" []
         (layout/add_user (get_users)))
    
    (GET "/single_person-page" [employee_id]
         (layout/single_person (get_worker employee_id)))
     
    (GET "/single_plane-page" [plane_id]     
        (let [sumcnt    (str (get (nth (take_sum_cnt plane_id) 0 "0") :sumсnt  "0"))
              air_time  (get (nth (get-plane_char plane_id) 0 "0") :air_time  "0")
              sum_plane (get (nth (get-plane_char plane_id) 0 "0") :sum_plane "0")
              sum_hours {:air_time (show_hours plane_id) :sum_plane (show_minutes plane_id)}]

          (if (= sum_plane nil) (def sum_plane 0) sum_plane)
          (layout/single_plane (get-plane_char plane_id) (get-plane_logs plane_id) (show_hours plane_id) (show_minutes plane_id))))
     
    (GET "/logout" [email :as req]
         (offline (:mail (:session req)))
         (println (str "mail: " (:mail (:session req))))
         (logout)
         (redirect "/login"))
     
    (GET "/change_plane-gage" []
         (layout/change_plane-gage))

    (GET "/delete_worker" [id]
      (del_it/delete_worker id)
      (redirect "/peolple-page"))
          
     
    (POST "/add_worker"
          [id Ename surname patronymic age spec email standing number 
                country town education exp skills pasport come_date dismissal_date]
          (if (= id nil)
                (add-empl Ename surname patronymic age spec email standing number 
                country town education exp skills pasport come_date "")
                (update-empl id Ename surname patronymic age spec email come_date dismissal_date))
          (redirect "/peolple-page"))
       
    (POST "/admin_add_plane" 
          [pl_name pl_number cr_date pl_l_upd a_time l_count 
          plane_number pilot s_pilot use_date use_time :as req]

          (if (= use_time nil) 
              (add-plane pl_name pl_number cr_date pl_l_upd a_time l_count "ready") 
              (add-log plane_number pilot s_pilot use_date use_time)
              ; (println (select-plane plane_number))
              )
          (if (= use_time nil)
              (redirect "/planes")
              (redirect "/logs")))
          
    (POST "/add_user"
          [id Uname surname role email pass]
           (if (= id nil)
                 (add-user Uname surname email role pass)
                 (update-user id Uname surname role email pass))
          (redirect "/peolple-page"))
      
    (POST "/add_change-logs"
          [plane_number pilot s_pilot use_date use_time squawk]
          ; (def air_time  (str(get (nth (get-plane_char_by_plnumber (str plane_number)) 0 "incorrect") :air_time  "incorrect")))
          ; (def sum_plane (str(get (nth (get-plane_char_by_plnumber (str plane_number)) 0 "incorrect") :sum_plane "incorrect")))
          
        ;   (add-log plane_number pilot s_pilot use_date use_time squawk)
          (redirect "/logs")
          
          (if (= squawk nil) 
            ;   (update-log plane_number pilot s_pilot use_date use_time)
              (println "update plane")
              (add-log plane_number pilot s_pilot use_date use_time squawk)))
      
    (POST "/login"
          [email pass]
          (let [log (str(get (nth (check_login (str email) (str pass)) 0 "incorrect") :user_password "incorrect"))]

            (if (= log "incorrect")
              "incorrect password or email"
              (and (online email)
                     (assoc (redirect "/") :session {:mail email :password pass})))))
    
    (POST "/registration"
          [email pname surname pass spass]
          (let [uemail (str(get (nth (check-useremail (str email)) 0 "incorrect") :user_email "incorrect"))]
            (if (= pass spass) 
                (if (= uemail email)
                       "email already exists"
                       (and (add-user pname surname email "user" pass) (redirect "/peolple-page")))
                "incorrect second password")))
     
    
    (route/resources "/")
    (compojure.route/not-found (layout/page-404)))

(def app (wrap-params myroutes))