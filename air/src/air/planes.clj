(ns air.planes
  (:require
    [clojure.java.jdbc :as sql]
    [air.database :refer [db]]))



(defn get-planes []
    "Gets top planes from database"
    (sql/query db ["SELECT * FROM planes;"]))

(defn get-plane_char [plane_id]
    "Gets single plane by id from database"
    (sql/query db ["SELECT * FROM planes LEFT JOIN sum_hours ON (planes.plane_number = sum_hours.pl_number) WHERE plane_id=?" plane_id]))

(defn get-plane_char_by_plnumber [pl_number]
    "Gets single plane by the privat number if plane from database"
    (sql/query db ["SELECT * FROM planes LEFT JOIN sum_hours ON (planes.plane_number = sum_hours.pl_number) WHERE pl_number=?" pl_number]))

(defn get-plane_logs [plane_id]
    "Gets plane logs from database"
    (sql/query db ["SELECT log_id, plane_number, plane_name, pilot,s_pilot, use_date, use_time, squawk FROM planes LEFT JOIN use_log  ON (planes.plane_number = use_log.pl_number) WHERE plane_id=?" plane_id]))
  
(defn get-services []
    "Gets top services from database"
    (sql/query db ["SELECT * FROM service_type;"]))
  
(defn get_users []
    "Gets all users from database"
    (sql/query db ["SELECT * FROM users;"]))

(defn get_workers []
    "Gets all workers from database"
    (sql/query db ["SELECT * FROM employees;"]))

(defn get_worker [id]
    "Gets worker by id from database"
    (sql/query db ["SELECT * FROM employees WHERE emp_id=?" id]))
  
(defn get_pilots []
    "Gets all pilots from database"
    (sql/query db ["SELECT * FROM pilots;"]))

(defn get_logs []
    "Gets all logs from database"
    (sql/query db ["SELECT * FROM use_log LEFT JOIN planes  ON (planes.plane_number = use_log.pl_number);"]))

(defn check_login [email pass]
    "check personal cridentials"
    (sql/query db ["SELECT user_password FROM users WHERE user_password=? AND user_email=?" pass email]))

(defn check-useremail [email]
    "check if the email exists..."
    (sql/query db ["SELECT user_email FROM users WHERE user_email=?" email]))

(defn take_sum_cnt [plane_id]
    "gets sum all time in the air"
    (sql/query db ["SELECT sum(cnt) AS  sumсnt FROM (
                        SELECT sum(use_time)  AS cnt  FROM use_log WHERE pl_number='43454453'
                        UNION
                        SELECT sum(air_time) AS cnt FROM planes WHERE plane_number='43454453'
                    ) icnt;"]))
  
  
; (defn join-plane_log
;     []
;     (sql/query db "SELECT * FROM planes LEFT JOIN use_log  ON (planes.plane_id = use_log.plane_id);"))


; SELECT * FROM planes LEFT JOIN planes_hours  ON (planes.plane_number = planes_hours.pl_number);
  
